 /*   
 * Project: OSMP
 * FileName: TestNettyClient.java
 * version: V1.0
 */
package com.osmp.netty.client.test;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.osmp.intf.define.server.Request;
import com.osmp.intf.define.server.Response;
import com.osmp.netty.client.NettyClient;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2017年2月8日 上午11:22:42上午10:51:30
 */
public class TestNettyClient {
	public static final String CONFIG_FILE = "classpath:META-INF/spring/applicationContext.xml";
	
	public static void main(String[] args) throws InterruptedException {
		AbstractApplicationContext application = new ClassPathXmlApplicationContext(CONFIG_FILE);
		application.start();
		NettyClient client = (NettyClient) application.getBean("nettyClient");
		client.connect();
		Map<String, String> source = new HashMap<String,String>();
		source.put("from", "clientTT");
		
		
		for(int i=0;i<20;i++){
			Map<String, Object> parameter = new HashMap<String,Object>();
			parameter.put("name", "我的名字叫"+i);
			parameter.put("age", i+"");
			
			Request request = new Request(UUID.randomUUID().toString(),source,"osmp-test",parameter);
			Response resp = client.sent(request);
			System.out.println(resp.getMsgId()+"_" + resp.getResult());
			Thread.sleep(1000*1);
		}
		client.close();
		application.close();
		application.stop();
	}

}
