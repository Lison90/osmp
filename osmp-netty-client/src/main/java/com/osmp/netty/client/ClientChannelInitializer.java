 /*   
 * Project: OSMP
 * FileName: ClientChannelInitializer.java
 * version: V1.0
 */
package com.osmp.netty.client;

import org.springframework.stereotype.Component;

import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2017年2月7日 下午4:31:22上午10:51:30
 */
@Component
public class ClientChannelInitializer  extends NettyClientChannelInitializer {
	
	@Override
	protected void initChannel(final SocketChannel ch) throws Exception {
		ch.pipeline().addLast(new ObjectEncoder());
		ch.pipeline().addLast(new ObjectDecoder(ClassResolvers.cacheDisabled(this.getClass().getClassLoader())));
		super.initChannel(ch);
	}
}
