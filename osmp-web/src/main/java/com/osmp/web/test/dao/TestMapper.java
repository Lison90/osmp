package com.osmp.web.test.dao;

import com.osmp.web.core.mybatis.BaseMapper;
import com.osmp.web.test.entity.Test;

public interface TestMapper extends BaseMapper<Test> {

}
