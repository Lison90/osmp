 /*   
 * Project: OSMP
 * FileName: DemoServiceImpl.java
 * version: V1.0
 */
package com.osmp.demo.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.osmp.demo.service.user.UserService;
import com.osmp.intf.define.model.Parameter;
import com.osmp.intf.define.service.BaseDataService;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2016年11月9日 下午5:31:09上午10:51:30
 */
public class DemoServiceImpl implements BaseDataService{

	@Autowired
	private UserService service;
	
	@Override
	public Object execute(Parameter parameter) {
		Object result = null;
		String op = parameter.getQueryMap().get("op");
		Map<String,Object> paramMap = new HashMap<String, Object>();
		paramMap.putAll(parameter.getQueryMap());
		switch (op) {
		case "cud":
			result = service.cudUser(paramMap);
			break;
		case "query":
			result = service.queryList(paramMap);
			break;
		case "queryPage":
			result = service.queryListByPage(paramMap);
			break;
		default:
			break;
		}
		
		return result;
	}

}
